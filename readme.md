<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Requirement
-> php 7.2.34
kami menyarankan menggunakan laragon

## Installation

```bash
# Ubah .env.example ke .env
# Ubah DB_CONNECTION,DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD sesuai database anda

# Tambahkan aplikasi composer
$ composer update

# Generate key application
$ php artisan key:generate

# Generate untuk migrasi database
$ php artisan migrate

# membuat seeder untuk db
$ php artisan db:seed

# Generate untuk menjalankan aplikasi ke localhost (default: localhost::8000)
$ php artisan serve


```
