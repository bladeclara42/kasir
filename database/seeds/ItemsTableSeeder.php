<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            $items = [
                [
                    'item_category_id' => 1,
                    'name' => 'Sabun Batang',
                    'price' => 3000,
                    'stock' => 50,
                    'image' => 'sabun.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 2,
                    'name' => 'Mi Instan',
                    'price' => 2000,
                    'stock' => 30,
                    'image' => 'mi_instan.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 1,
                    'name' => 'Pensil',
                    'price' => 1000,
                    'stock' => 30,
                    'image' => 'pensil.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 2,
                    'name' => 'Pasta Gigi',
                    'price' => 5000,
                    'stock' => 30,
                    'image' => 'pasta_gigi.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 1,
                    'name' => 'Shampoo',
                    'price' => 8000,
                    'stock' => 40,
                    'image' => 'shampoo.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 2,
                    'name' => 'Kopi Sachet',
                    'price' => 1500,
                    'stock' => 30,
                    'image' => 'kopi_sachet.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 1,
                    'name' => 'Tissue Toilet',
                    'price' => 5000,
                    'stock' => 60,
                    'image' => 'tissue_toilet.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 2,
                    'name' => 'Susu Kaleng',
                    'price' => 10000,
                    'stock' => 25,
                    'image' => 'susu_kaleng.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 1,
                    'name' => 'Penghapus',
                    'price' => 500,
                    'stock' => 35,
                    'image' => 'penghapus.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'item_category_id' => 2,
                    'name' => 'Coklat Batang',
                    'price' => 2000,
                    'stock' => 70,
                    'image' => 'coklat_batang.jpg',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
            ],
            // Tambahkan item lainnya di sini
            // ...
        ];

        DB::table('items')->insert($items);
    }
}
